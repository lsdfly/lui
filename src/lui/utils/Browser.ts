let ua: any = window.navigator.userAgent.toLowerCase();
export const isWeiXin = () => {
    return ua.match(/MicroMessenger/i) == 'micromessenger';
} 