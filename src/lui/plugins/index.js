import Vue from 'vue'
import MDialog from './MDialog'
// import MAddress from '@/lui/plugins/MAddress'
import Loading from './Loading'
import Toast from './Toast'
import DateTime from './DateTime';

Vue.use(MDialog);
// Vue.use(MAddress);
Vue.use(Loading);
Vue.use(Toast);
Vue.use(DateTime);