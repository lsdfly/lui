import Vue from 'vue'

declare module 'vue/types/vue' {
    interface Vue {
        $toast: {
            text:Function
            show:Function
            hide:Function
        }
       
    }
}