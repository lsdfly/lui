import Vue from 'vue';

import 'lib-flexible'
import '@/lui/layout/index.less'
import lui from '@/lui'
Vue.use(lui);
import App from './App.vue';
import router from './router';
const FastClick = require('fastclick')
FastClick.attach(document.body)

Vue.config.productionTip = false;

new Vue({
  router,
  // store,
  render: h => h(App),
}).$mount('#app');
