import Vue from 'vue';
import Router from 'vue-router';



Vue.use(Router);
let router = new Router({
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
 
  ],
});

router.beforeEach((to: any, from: any, next: Function) => {
 
})
export default router